import { useContext, Fragment } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';
import logo from './images/cascara-logo.png';


export default function AppNavbar(){
	/*
		localStorage.getItem is used  to get a piece of information from the browser's local storage. devs usually insert information in the local storage for specific purposes; one of commonly used purpose is for login in. it accepts 1 argument pertaining to the specific key/property inside the local storage to get its value
	*/
	// uses the state hook to set the initial value for user variable coming from the local storage, specifically the email property
	const { user } = useContext(UserContext);
	console.log(user);

	return(
			<Navbar bg="dark" variant="dark" expand="lg" sticky='top'>
				<Navbar.Brand as={ Link } to='/' className='p-3' >
                <img
                src={logo} 
                width="50"
                height="50"
                className="d-inline-block align-top"
                alt="CASCARA LOGO"
            /></Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto p-3">
						<Nav.Link as={ NavLink } to='/' exact>Home</Nav.Link>
						<Nav.Link as={ NavLink } to='/products' exact> Products </Nav.Link>
						{/*
							Miniactivity
								- using Fragment, user email, and ternary operator, perform the following:
									1. if the user email is existing, show the logout navigation
									2. if the user email does not exist, show the login and register navigations

									feel free to send the output in batch google chat (logged in user)
						*/}
						{(user.id !== null) ?
						<Fragment>
							<Nav.Link as={ NavLink } to='/logout' exact> Logout</Nav.Link>
							<Nav.Link as={ NavLink } to='/:userId/checkout' exact> Cart </Nav.Link>
						</Fragment>

						:
							<Fragment>
								<Nav.Link as={ NavLink } to='/login' exact> Login</Nav.Link>
								<Nav.Link as={ NavLink } to='/register' exact> Register</Nav.Link>
							</Fragment>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}