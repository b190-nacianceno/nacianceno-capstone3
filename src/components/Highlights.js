import { Row, Col, Card } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import atokPhoto from '../images/1.jpg';
import sagadaPhoto from '../images/2.jpg';
import mankayanPhoto from '../images/3.jpg';
import kalingaPhoto from '../images/4.jpg';
// ctrl + shift + p
export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4} lg={3}>
				<Card className="cardHighlight p-3">

				<a href='/products'>

					<img src={atokPhoto} width="100%"/>
				</a>
				
				
				</Card>
			</Col>
			<Col xs={12} md={4} lg={3}>
				<Card className="cardHighlight p-3">

				<a href='/products'>
				<img src={sagadaPhoto} width="100%"/>
				</a>
				</Card>
			</Col>
			<Col xs={12} md={4} lg={3}>
				<Card className="cardHighlight p-3">
				<a href='/products'>
				<img src={mankayanPhoto} width="100%"/>
				</a>
				</Card>
			</Col>
			<Col xs={12} md={4} lg={3}>
				<Card className="cardHighlight p-3">
				<a href='/products'>
				<img src={kalingaPhoto} width="100%"/>
				</a>
				</Card>
			</Col>
		</Row>
	)
}