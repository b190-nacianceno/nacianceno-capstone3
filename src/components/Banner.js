// react permits the use of object literals if the dev will import multiple components from the same dependency
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	const { title, content, destination, label } = data;
	return(
			<Row>
				<Col className="p-5">
					<h1>{title}</h1>
					<p>{content}</p>
                    <Button href="/products">{label}</Button> 

				</Col>
			</Row>
		)
}