import { Card, Button, Container, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

    console.log(productProp);    
    console.log(typeof productProp);

   
    const { name, description, price, _id } = productProp;


    return (
        // <Container>
        // <Row className="mt-3 mb-3">
        //     <Col xs={12} md={6} lg={6} className="content-center">
                <Card className='m-5'>
                
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>₱{price}</Card.Text>

                        <Link className='btn btn-primary' to={`/products/${_id}`}>More Details</Link>


                    </Card.Body>
                </Card>
        //     </Col>
        // </Row>

        // </Container>
    )
}
