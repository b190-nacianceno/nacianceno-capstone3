import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import atokPhoto from '../images/1.jpg';
import sagadaPhoto from '../images/2.jpg';
import mankayanPhoto from '../images/3.jpg';
import kalingaPhoto from '../images/4.jpg';

import UserContext from '../UserContext';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	const checkout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/checkout`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Item has been added to cart",
					icon: "success",
					text: "You have successfully added the item in your cart!"
				});
				navigate('/user/checkout');
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>₱{price}</Card.Text>

							<CardImg></CardImg>
							

							{ (user.id !== null)?
							<Button variant="primary" onClick={()=>checkout(productId)} block>Add to Cart</Button>
							:
							<Link className='btn btn-danger btn-block' to='/login'>Login to Add to Cart</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)



}