import imageBanner from '../images/hand.jpg';

export default function(){

    return (

    <img src={imageBanner}
    width="100%"
    height="auto"
    className="center"
    alt="hands holding coffee"/>
    );
}