import './App.css';

// UserContext
import { UserProvider } from './UserContext';

import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Web Components
import AppNavbar from './AppNavbar';

// Web Pages

import Home from './pages/Home';
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Cart from './components/Cart';
// import Error from "./pages/Error";


function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
  useEffect(() => {

  // console.log(user);

  fetch('http://localhost:4000/user/details', {
    method:'GET',
    headers: {
      Authorization: `Bearer ${ localStorage.getItem('token') }`
    }
  })
  .then(res => res.json())
  .then(data => {

    // Set the user states values with the user details upon successful login.
    if (typeof data._id !== "undefined") {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });

    // Else set the user states to the initial values
    } else {

      setUser({
        id: null,
        isAdmin: null
      });

    }

  })

  }, []);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/products' element={<Products />} />
          <Route path='/products/:productId' element={<ProductView />} />
          <Route path='/user/checkout' element={<Cart />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          {/* <Route path='*' element={<Error />} /> */}
        </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}


export default App;