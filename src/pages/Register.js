import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import registerPhoto from '../images/iced-coffee.jpg';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

		
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [email, setEmail] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		
		const [isActive, setIsActive] = useState(false);

		
		console.log(email);
		console.log(password1);
		console.log(password2);
		
	useEffect(()=>{
		if( ( firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '' ) && ( password1 === password2 ) ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [ firstName, lastName, mobileNo, email, password1, password2 ]);

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){
		    	Swal.fire({
		    		title: "Duplicate	email found",
		    		icon: "error",
		    		text: "Kindly provide another email address."
		    	})
		    }else{
		    	fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        firstName: firstName,
		    	        lastName: lastName,
		    	        email: email,
		    	        mobileNo: mobileNo,
		    	        password: password1
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data === true) {

		    	        // Clear input fields
		    	        setFirstName('');
		    	        setLastName('');
		    	        setEmail('');
		    	        setMobileNo('');
		    	        setPassword1('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: 'Welcome to Zuitt!'

		    	        });
	    	            navigate('/login')

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something wrong',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	    }
		    	})
		    }
		})
	}

	return (
		(user.id !== null) ?
		<Navigate to='/products' />
		:

        // Start Card
        <Card className='m-2 mt-5'>
        {/* Form Row */}
        <Row>
        <Col>
        
	  <Form onSubmit={(e) => registerUser(e)} className="p-5">
        <h1 className='m-2'>Register</h1>
	    <Form.Group controlId="userEmail" className='m-2'>
	      <Form.Label>First Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter first name" 
	      				value={firstName}
	      				onChange={e => setFirstName(e.target.value)}
	      				required 
			/>
	     
	    </Form.Group>

	    <Form.Group controlId="userEmail" className='m-2'>
	      <Form.Label>Last Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter last name" 
	      				value={lastName}
	      				onChange={e => setLastName(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="userEmail" className='m-2'>
	      <Form.Label>Mobile Number</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Enter mobile number" 
	      				value={mobileNo}
	      				onChange={e => setMobileNo(e.target.value)}
	      				required 
			/>
	      
	    </Form.Group>

	    <Form.Group controlId="userEmail" className='m-2'>
	      <Form.Label>Email address</Form.Label>
	      <Form.Control 
	      				type="email" 
	      				placeholder="Enter email" 
	      				value={email}
	      				onChange={e => setEmail(e.target.value)}
	      				required 
			/>
	     
	    </Form.Group>

	    <Form.Group controlId="password1" className='m-2'>
	      <Form.Label>Password</Form.Label>
	      <Form.Control
	      				type="password" 
	      				placeholder="Password" 
	      				value={password1}
	      				onChange={e => setPassword1(e.target.value)}
	      				required
			/>
	    </Form.Group>

	    <Form.Group controlId="password2" className='m-2'>
	      <Form.Label>Verify Password</Form.Label>
	      <Form.Control 
	      				type="password" 
	      				placeholder="Password" 
	      				value={password2}
	      				onChange={e => setPassword2(e.target.value)}
	      				required
			/>
	    </Form.Group>
	    {isActive ?
	    <Button variant="primary" type="submit" id="submitBtn" className='m-2'>Submit</Button>
	    :
	    <Button variant="primary" type="submit" id="submitBtn" className='m-2' disabled>Submit</Button>
		}
	  </Form>
        </Col>

        <Col>
        <img src={registerPhoto} width="100%" className=''/>
        </Col>

        </Row>

        {/* Image row */}
        



        

        </Card>
	);
}