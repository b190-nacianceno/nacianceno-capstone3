import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container,Row, Col,Card} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import React from 'react';
import loginphoto from '../images/bagbeans.jpg';


import Swal from 'sweetalert2';

export default function Login() {

   

	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive] = useState(false);


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            method: 'GET',
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();

		
		fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title:"Login Successful!",
					icon: "sucess",
					text: "Welcome to Zuitt!"
				})
			} else{
				Swal.fire({
					title: "Authentication Failed.",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});

		

		setEmail('');
		setPassword('');
	}


	return(
		(user.id !== null) ?
		<Navigate to='/products' />
		:

          <Card className='m-2 mt-5' >
          <Container>
          <Row>
          <Col>

          <img src={loginphoto} width='700px' className='p-3'/>

          </Col>
          <Col>

        
		  <Form onSubmit={(e) => loginUser(e)} className="p-3 m-2">
          <h1 className='m-2 pb-2'>Login</h1>
		    <Form.Group controlId="userEmail" className='m-2'>
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
		      
		    </Form.Group>

		    <Form.Group controlId="password1" className='m-2'>
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password}
		      				onChange={e => setPassword(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    {isActive ?
		    <Button variant="primary" type="submit" id="submitBtn" className='m-2'>Login</Button>
		    :
		    <Button variant="primary" type="submit" id="submitBtn" className='m-2' disabled>Login</Button>
			}
		  </Form>
          </Col>

          </Row>

          </Container>

          </Card>

    
  );
  }
