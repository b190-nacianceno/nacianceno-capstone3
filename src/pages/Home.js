import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Image from '../components/Image';

export default function Home(){
	const data = {
        
		title: "Cascara Coffee",
		content: "Single origin coffee sourced directly from our Filipino farmers.",
		destination: "/products",
		label: "Shop Now!"
	}
	return(
		<Fragment>
			<Banner data={data} />
            <Image />
			<Highlights />
		</Fragment>
	)
}