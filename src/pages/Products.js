import { Fragment, useEffect, useState, useContext } from 'react';

import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';

export default function Products() {

   

    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);

    //Function to fetch our courses data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
    const fetchData = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products`)
        .then(res => res.json())
        .then(data => {

            setProducts(data);

        });

    }

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular courses page. */}
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
        </Fragment>
    )

}